#!/bin/bash
set -eu

mkdir -p /run/greenlight/tmp/pids /run/greenlight/log /app/data/storage

if [[ ! -f /app/data/env ]]; then
    cp /app/pkg/env.template /app/data/env
    secret_key_base="$(bundle exec rails secret)"
    sed -e "s/SECRET_KEY_BASE=.*/SECRET_KEY_BASE=\"$secret_key_base\"/" -i /app/data/env
fi

export URL_HOST="${CLOUDRON_APP_ORIGIN}"
export DATABASE_URL="${CLOUDRON_POSTGRESQL_URL}"
export REDIS_URL="redis://${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}"

export SMTP_SERVER="${CLOUDRON_MAIL_SMTP_SERVER}"
export SMTP_PORT="${CLOUDRON_MAIL_SMTP_PORT}"
export SMTP_DOMAIN="${CLOUDRON_MAIL_DOMAIN}"
export SMTP_USERNAME="${CLOUDRON_MAIL_SMTP_USERNAME}"
export SMTP_PASSWORD="${CLOUDRON_MAIL_SMTP_PASSWORD}"
export SMTP_AUTH=login
export SMTP_STARTTLS_AUTO=true
export SMTP_SENDER_EMAIL="${CLOUDRON_MAIL_FROM}"
export SMTP_SENDER_NAME="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Greenlight}"

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    export OPENID_CONNECT_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export OPENID_CONNECT_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
    export OPENID_CONNECT_ISSUER="${CLOUDRON_OIDC_ISSUER}"
    export OPENID_CONNECT_REDIRECT="${CLOUDRON_APP_ORIGIN}"
    export OPENID_CONNECT_UID_FIELD=sub
fi

# rails env
export RAILS_ENV=production
export RAILS_LOG_TO_STDOUT=true
export RAILS_SERVE_STATIC_FILES=true

# Export all variables in /app/data/env
echo "==> sourcing /app/data/env"
export $(grep -v '^#' /app/data/env | xargs)

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/greenlight

table_count=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -AXqtc "SELECT COUNT(*) FROM pg_tables WHERE schemaname = 'public';")
if [[ "${table_count}" == "0" ]]; then
    echo "==> Initializing database"
    gosu cloudron:cloudron bundle exec rails db:migrate
    gosu cloudron:cloudron bundle exec rails db:migrate:with_data

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo "==> making Administrator as Default Role"
        echo "SiteSetting.find_by(setting: Setting.find_by(name: 'DefaultRole'), provider: 'greenlight').update(value: 'Administrator')" | gosu cloudron:cloudron bundle exec rails console
    else
        # Create default admin user
        echo "==> Creating admin user"
        gosu cloudron:cloudron bundle exec rake admin:create['admin','admin@cloudron.local','Changeme!123']
    fi
else
    echo "==> Migrating database"
    gosu cloudron:cloudron bundle exec rails db:migrate
fi

echo "==> Starting Greenlight"
exec gosu cloudron:cloudron bundle exec puma -C config/puma.rb -p 3000
