FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg

RUN chown -R cloudron:cloudron /app/code

RUN npm install --no-update-check -g yarn

USER cloudron

# https://github.com/bigbluebutton/greenlight/blob/master/dockerfiles/v3/amazonlinux#L30
ARG RBENV_PATH=/home/cloudron/.rbenv
ARG RUBY_VERSION=3.3.6
RUN curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash
ENV PATH="${RBENV_PATH}/bin:${RBENV_PATH}/versions/${RUBY_VERSION}/bin:${PATH}"

RUN rbenv install ${RUBY_VERSION} && \
    rbenv global ${RUBY_VERSION}

WORKDIR /app/code

RUN gem install bundler -v '~> 2.5'

# renovate: datasource=github-releases depName=bigbluebutton/greenlight versioning=semver extractVersion=^release-(?<version>.+)$
ARG GREENLIGHT_VERSION=3.5.2

RUN curl -L https://github.com/bigbluebutton/greenlight/archive/refs/tags/release-${GREENLIGHT_VERSION}.tar.gz | tar -zxvf - --strip-components=1 -C /app/code

RUN bundle install -j4 && \
    yarn install && \
    yarn cache clean

RUN bundle exec --verbose rake assets:precompile --trace --verbose

RUN rm -rf /app/code/tmp && ln -sf /run/greenlight/tmp /app/code/tmp && \
    rm -rf /app/code/log && ln -sf /run/greenlight/tmp/log /app/code/log && \
    rm -rf /app/code/storage && ln -s /app/data/storage /app/code/storage

USER root

COPY env.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
