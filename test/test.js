#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';

    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const adminEmail = 'admin@cloudron.local';
    const adminPassword = 'Changeme!123';

    const ROOM_ID = Math.floor((Math.random() * 100) + 1);
    const ROOM_NAME = 'Test room ' + ROOM_ID;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/signin`);
        await waitForElement(By.id('signInFormEmail'));
        await browser.findElement(By.id('signInFormEmail')).sendKeys(username);
        await browser.sleep(2000);
        await browser.findElement(By.id('signInFormPwd')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(text(), "Sign In")]')).click();
        await browser.sleep(2000);

        await waitForElement(By.xpath('//button[contains(., "New Room")]'));
    }

    async function loginOIDC(username, password, alreadyAuthenticated=true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[contains(., "Welcome to BigBlueButton")]'));

        await browser.findElement(By.xpath('//div[contains(@class,"hstack")]//button[contains(text(), "Sign In")]')).click();
        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await browser.sleep(2000);
        await waitForElement(By.xpath('//button[contains(., "New Room")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[@id="nav-user-dropdown"]'));
        await browser.findElement(By.xpath('//a[@id="nav-user-dropdown"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[@aria-labelledby="nav-user-dropdown"]//button[text()="Sign Out"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h1[contains(., "Welcome to BigBlueButton")]'));
    }

    async function createRoom() {
        await browser.get(`https://${app.fqdn}/rooms`);
        await waitForElement(By.xpath('//button[contains(text(), "+ New Room")]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(text(), "+ New Room")]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@id="createRoleFormName"]')).sendKeys(ROOM_NAME);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Create Room"]')).click();
        await browser.sleep(2000);
        await browser.get(`https://${app.fqdn}/rooms`);
        await waitForElement(By.xpath(`//div[@id="room-card"]/div[contains(., "${ROOM_NAME}")]`));
    }

    async function checkRoom() {
        await browser.get(`https://${app.fqdn}/rooms`);
        await waitForElement(By.xpath(`//div[@id="room-card"]/div[contains(., "${ROOM_NAME}")]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app (No SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminEmail, adminPassword));

    it('can create room', createRoom);
    it('can check room', checkRoom);

    it('can admin logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app (SSO)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD, false));

    it('can create room', createRoom);
    it('can check room', checkRoom);

    it('can logout', logout);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`);
    });

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can check room', checkRoom);
    it('can logout', logout);

    it('backup app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron backup create --app ${app.id}`);
    });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can check room', checkRoom);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));
    it('can check room', checkRoom);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.bigbluebutton.greenlight3.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));

    it('can create room', createRoom);
    it('can check room', checkRoom);

    it('can logout', logout);

    it('can update', async function () {
        await browser.get('about:blank');
        execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can OIDC login', loginOIDC.bind(null, USERNAME, PASSWORD));

    it('can check room', checkRoom);

    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
