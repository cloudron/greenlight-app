<sso>
By default, Cloudron users have `Administrator` role permissions. This can be changed in `Administrator Panel` > `Site Settings` > `Registration`.
</sso>

<nosso>
This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin@cloudron.local<br/>
**Password**: Changeme!123<br/>

</nosso>

