Greenlight is the main front-end interface for a BigBlueButton server.

This app does **not** bundle BigBlueButton itself, but requires a backend server to be installed.
